<?php


$module_directory = 'tabber';
$module_name = 'Tabber';
$module_function = 'snippet';
$module_version = '1.0.0';
$module_platform = '4.x';
$module_author = 'BarelyFitz Designs // Greg Bosen';
$module_license = 'MIT License';
$module_guid	= '0210855b-5c1f-4c34-ae57-73c351be457c';
$module_description = 'http://www.barelyfitz.com/projects/tabber/ <br>usage: tabber("3,9,12"); <br>Comma seperated list of page_id\'s';

?>
