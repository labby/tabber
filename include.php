<?php
/* Tabber

   Call this snippet with:
        tabber (array(3,12,29)); //comma seperated list of page_id's
   or
        tabber('3,12,29');       //regard the quotes! 
   in a code page
   
   To use the menu title instead of the page title, call with:
   
       tabber_use_menu_title();
       tabber('3,5');
   
   To use sections instead of pages, call with:
   
   tabber_sections(
       array(
           array( 'id' => 10, 'title' => "Sektion 10" ),
           array( 'id' => 11, 'title' => "Sektion 11" )
       )
   );
   
   Original author "tomhung"
   
   Maintenance taken by Bianka Martinovic as of 2009-03-16
   
*/

include_once(WB_PATH.'/framework/summary.functions.php');

$use_menu_title = false;
$titles         = array();

/**
 * This is used to enable the use of menu titles as tab headers
 **/  
function tabber_use_menu_title () {
    global $use_menu_title;
    $use_menu_title = true;
}   // function tabber_use_menu_title

/**
 * Include the tabber
 **/ 
function tabber( $page_list = array() ) {

    global $oLEPTON;
    global $use_menu_title;
    global $titles;
        
    if ( ! is_array( $page_list ) ) {
        $page_list = explode( ',', $page_list ); 
    }
    
    if ( ! is_array( $page_list ) or count( $page_list ) == 0 ) {
        return NULL;
    }
    
  	_tabber_start();
  	
  	foreach ( $page_list as $page ) {
  	
  	    // prevent recursion
  	    if ( $page == $oLEPTON->page_id ) {
  	        continue;
        }
        
        if( isset( $titles[$page] ) ) {
            $title = $titles[$page];
        }
        elseif ( $use_menu_title ) {
            $title = get_menu_title($page);
        }
        else {
            $title = get_page_title($page);
        }
  	    
  	    ob_start();
  	    _tabber_get_page_content($page);
  	    $content = ob_get_contents();
			  ob_end_clean();
  	    
  		  _tabber_item( $title, $content );
  	
    }
  	
    echo '</div>';
    
}   // end function tabber

/**
 *
 **/ 
function tabber_page_titles ( $set_titles = array() ) {
    global $titles;
    $titles = $set_titles;
    return;
}   // end function tabber_page_titles

/**
 * Show tabber using sections
 **/
function tabber_sections( $section_list = array() ) {

    global $oLEPTON;

    if ( ! is_array( $section_list ) or count( $section_list ) == 0 ) {
        return NULL;
    }
    
    _tabber_start();
    
  	foreach ( $section_list as $section) {

  	    $content = _tabber_get_section($section['id']);
  	    $title   = $section['title'];
  	    
  	    if ( $content ) {
  	
            _tabber_item( $title, $content );
      		  
	      }
  	
    }
  	
    echo '</div>';
    
}   // end function tabber_sections


/***********************************************************************
 * INTERNAL
 **********************************************************************/

/**
 * Get page contents
 **/ 
function _tabber_get_page_content ($get_id) {
	
    global $oLEPTON;
        
    // store original page id
	  $me_id = $oLEPTON->page_id;
 
    //echo "current page id: $me_id<br />get_id: $get_id<br /><br />";
	  
	  // prevent recursion
	  if ( $me_id == $get_id ) {
	      return;
    }
    
    // override page id to get the page
	  $oLEPTON->page_id = $get_id;
	  
	  // this will cause NOTICEs 'cause get_page_details() defines
	  // lots of constants, so I use a very dirty trick here at 
    // the moment to prevent these NOTICEs from being printed
    @$oLEPTON->get_page_details();
    
    // get the page content 
	  page_content();
    
    // restore original page id
	  $oLEPTON->page_id = $me_id;
	  
	  // really really dirty...
	  @$oLEPTON->get_page_details();
	  
}   // end function _tabber_get_page_content

/**
 * Get sections contents
 **/ 
function _tabber_get_section($id) {

    global $oLEPTON, $database;

    // get the section data
    $result = $database->query("SELECT section_id,module,publ_start,publ_end FROM ".TABLE_PREFIX."sections WHERE section_id = '".$id."'");
    
    if ( $section = $result->fetchRow() ) { 

				// skip this section if it is out of publication-date
				$now = time();
				if( !(($now<=$section['publ_end'] || $section['publ_end']==0) && ($now>=$section['publ_start'] || $section['publ_start']==0)) ) {
					  return;
				}
				
				$section_id = $section['section_id'];
				$module     = $section['module'];
				
				ob_start(); //start output buffer
				require(WB_PATH.'/modules/'.$module.'/view.php');
				$foo = ob_get_contents(); // put outputbuffer in $foo
				ob_end_clean();           // clear outputbuffer

        return $foo;
    
    }

}   // end function _tabber_get_section

/**
 * Tabber heading
 **/ 
function _tabber_start() {

   	echo '<link rel="stylesheet" href="'.WB_URL.'/modules/tabber/frontend.css" type="text/css" media="screen" />';
  	echo '<script type="text/javascript" src="'.WB_URL.'/modules/tabber/cookie.js"></script>';
  	echo '<script type="text/javascript" src="'.WB_URL.'/modules/tabber/tabber.js"></script>';
  	echo '<div class="tabber">';

}

/**
 * Tabber tab
 **/ 
function _tabber_item( $title, $content ) {
    echo '<div class="tabbertab">';
  	echo "<h2>". $title ."</h2>";
  	echo '<div style="padding:5px;">';
  	echo $content;
  	echo "</div>";
  	echo "</div>";
}

/*

   HISTORY
   
   0.6 - Some CSS rework         
   
   0.5 - Added $oLEPTON to globals in _tabber_get_section
   
   0.4 - * New: Use sections
         * New: Set tab titles for pages
      
   0.3 - Allow "menu title" of pages to be used instead of "page title"
   
   0.2 - First maintenance release by Bianka Martinovic
         * Fix: Sections after tabber inclusion disappeared from the page
         * Fix: Prevent recursion (if the page the tabber is included in appears in the page list)
         * Fix: Suppress PHP NOTICEs (Quick and dirty at the moment; I didn't find a clean way in this short time)
         * Renamed function get_page_content() to _tabber_get_page_content() to be more precise (avoid name clash)
         * Renamed tabber.css to frontend.css
         * Allows
               tabber(array(1,2,3));
           or
               tabber('1,2,3');
          to be used
        * Corrected HTML to be valid XHTML
   
   0.1 - First release by "tomhung"

*/
?>
